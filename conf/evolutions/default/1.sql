# --- !Ups

CREATE TABLE IF NOT EXISTS users (
	id SERIAL PRIMARY KEY,
	nickname VARCHAR ( 64 ) NOT NULL,
	password VARCHAR ( 64 ) NOT NULL,
	money INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS bets (
  id SERIAL PRIMARY KEY,
  first_user_id INTEGER REFERENCES users (id) NOT NULL,
  first_prediction NUMERIC NOT NULL,
  second_user_id INTEGER REFERENCES users (id),
  second_prediction NUMERIC,
  bet_money INTEGER NOT NULL,
  stock VARCHAR ( 4 ) NOT NULL,
  call_expiration_date TIMESTAMP NOT NULL,
  pred_expiration_date TIMESTAMP NOT NULL,
  bet_status VARCHAR ( 10 ) NOT NULL,
  winner_id INTEGER REFERENCES users (id)
);

# --- !Downs

