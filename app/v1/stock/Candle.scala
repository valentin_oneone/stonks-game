package v1.stock

import java.time.LocalDateTime

import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsPath, Json, Reads}

case class CandlesResponse(trackingId: String, payload: List[Candle] = List())

object CandlesResponse {
  implicit val reads: Reads[CandlesResponse] = (
    (JsPath \ "trackingId").read[String] and
      (JsPath  \ "payload" \ "candles").read[List[Candle]]
  ) (CandlesResponse.apply _)
}

case class Candle(o: Double,
                  c: Double,
                  h: Double,
                  l: Double,
                  v: Double,
                  time: LocalDateTime,
                  interval: String,
                  figi: String)

object Candle {
  implicit val candleReads: Reads[Candle] = Json.reads[Candle]
}
