package v1.user

import akka.actor.ActorSystem
import javax.inject.{Inject, Singleton}
import play.api.MarkerContext
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.concurrent.CustomExecutionContext
import play.api.libs.json.Json
import slick.jdbc.JdbcProfile
import exception.{NotEnoughMoney}

import scala.concurrent.{ExecutionContext, Future}

case class User(id: Int, nickname: String, password: String, money: Int)

object User {
  implicit val userFormat = Json.format[User]
}

class UserExecutionContext @Inject()(actorSystem: ActorSystem)
  extends CustomExecutionContext(actorSystem, "repository.dispatcher")

trait UserRepository {
  def create(nickname: String, password: String)(implicit mc: MarkerContext): Future[User]

  def list()(implicit mc: MarkerContext): Future[Seq[User]]

  def get(id: Int)(implicit mc: MarkerContext): Future[Option[User]]

  def withdrawal(id: Int, money: Int)(implicit mc: MarkerContext): Future[Unit]

  def refill(id: Int, money: Int)(implicit mc: MarkerContext): Future[Int]
}

@Singleton
class UserRepositoryImpl @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) extends UserRepository {
  private val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  private class UserTable(tag: Tag) extends Table[User](tag, "users") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)

    def nickname = column[String]("nickname")

    def password = column[String]("password")

    def money = column[Int]("money")

    override def * = (id, nickname, password, money) <>
      ((User.apply _).tupled, User.unapply)
  }

  private val users = TableQuery[UserTable]

  def create(nickname: String, password: String)(implicit mc: MarkerContext): Future[User] = db.run {
    (users returning users.map(_.id)
      into((user, id) => user.copy(id = id))
      ) += User(0, nickname, password, 10_000)
  }

  def list()(implicit mc: MarkerContext): Future[Seq[User]] = db.run {
    users.result
  }

  def get(id: Int)(implicit mc: MarkerContext): Future[Option[User]] = db.run {
    users.filter(_.id === id).result.headOption
  }

  def withdrawal(id: Int, money: Int)(implicit mc: MarkerContext): Future[Unit] = db.run {
    for {
      user <- users.filter(_.id === id).result.head
      updatedMoney <- if (user.money - money >= 0) users.filter(_.id === id)
        .map(user => user.money)
        .update(user.money - money) else DBIO.failed(new NotEnoughMoney(user.id))
    } yield ()
  }

  def refill(id: Int, money: Int)(implicit mc: MarkerContext): Future[Int] = db.run {
    for {
      user <- users.filter(_.id === id).result.head
      updatedMoney <- users.filter(_.id === id)
        .map(user => user.money)
        .update(user.money + money)
    } yield updatedMoney
  }
}

