package v1.user

import javax.inject.{Inject, Provider}
import play.api.MarkerContext
import play.api.libs.json._

import scala.concurrent.{ExecutionContext, Future}

case class UserResource(id: String, nickname: String, money: String)

object UserResource {
    implicit val format: Format[UserResource] = Json.format
}

class UserResourceHandler @Inject()(
    routerProvider: Provider[UserRouter],
    userRepository: UserRepositoryImpl)(implicit ec: ExecutionContext) {

  def create(userInput: UserFormInput)(
      implicit mc: MarkerContext): Future[UserResource] = {
    userRepository.create(userInput.nickname, userInput.password).map { user =>
      createUserResource(user)
    }
  }

  def find(id: String)(
      implicit mc: MarkerContext): Future[Option[UserResource]] = {
    val userFuture = userRepository.get(id.toInt)
    userFuture.map { maybePostData =>
      maybePostData.map { postData =>
        createUserResource(postData)
      }
    }
  }

  def find(implicit mc: MarkerContext): Future[Iterable[UserResource]] = {
    userRepository.list().map { postDataList =>
      postDataList.map(postData => createUserResource(postData))
    }
  }

  def withdrawal(id: String, withdrawalInput: WithdrawalFormInput)(
    implicit mc: MarkerContext): Future[Unit] = {
    userRepository.withdrawal(id.toInt, withdrawalInput.money)
  }

  private def createUserResource(u: User): UserResource = {
    UserResource(u.id.toString, u.nickname, u.money.toString)
  }

}
