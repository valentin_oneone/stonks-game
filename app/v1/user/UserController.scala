package v1.user

import javax.inject.Inject
import play.api.Logger
import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

case class UserFormInput(nickname: String, password: String)

case class WithdrawalFormInput(money: Int)

class UserController @Inject()(cc: UserControllerComponents)(
    implicit ec: ExecutionContext)
    extends UserBaseController(cc) {

  private val logger = Logger(getClass)

  private val userForm: Form[UserFormInput] = {
    import play.api.data.Forms._

    Form(
      mapping(
        "nickname" -> nonEmptyText,
        "password" -> nonEmptyText,
      )(UserFormInput.apply)(UserFormInput.unapply)
    )
  }

  private val withdrawalForm: Form[WithdrawalFormInput] = {
    import play.api.data.Forms._

    Form(
      mapping(
        "money" -> number,
      )(WithdrawalFormInput.apply)(WithdrawalFormInput.unapply)
    )
  }

  def index: Action[AnyContent] = UserAction.async { implicit request =>
    logger.trace("index: ")
    userResourceHandler.find.map { users =>
      Ok(Json.toJson(users))
    }
  }

  def process: Action[AnyContent] = UserAction.async { implicit request =>
    logger.trace("process: ")
    processJsonPost()
  }

  def show(id: String): Action[AnyContent] = UserAction.async {
    implicit request =>
      logger.trace(s"show: id = $id")
      userResourceHandler.find(id).map { post =>
        Ok(Json.toJson(post))
      }
  }

  def processWithdrawal(id: String): Action[AnyContent] = UserAction.async { implicit request =>
    logger.trace("process withdrawal")
    processWithdrawalJsonPost(id)
  }

  private def processJsonPost[A]()(
      implicit request: UserRequest[A]): Future[Result] = {
    def failure(badForm: Form[UserFormInput]) = {
      Future.successful(BadRequest(badForm.errorsAsJson))
    }

    def success(input: UserFormInput) = {
      userResourceHandler.create(input).map { user =>
        Created(Json.toJson(user)).withHeaders(LOCATION -> user.id)
      }
    }

    userForm.bindFromRequest().fold(failure, success)
  }

  private def processWithdrawalJsonPost[A](id: String)(implicit request: UserRequest[A]): Future[Result] = {
    def failure(badForm: Form[WithdrawalFormInput]) = {
      Future.successful(BadRequest(badForm.errorsAsJson))
    }

    def success(input: WithdrawalFormInput) = {
      for {
        user <- userResourceHandler.find(id)
        updated <- userResourceHandler.withdrawal(user.get.id, input)
      } yield Accepted
    }

    withdrawalForm.bindFromRequest().fold(failure, success)
  }
}
