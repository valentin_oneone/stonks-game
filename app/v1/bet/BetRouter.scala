package v1.bet

import javax.inject.Inject
import play.api.routing.Router.Routes
import play.api.routing.SimpleRouter
import play.api.routing.sird._

class BetRouter @Inject()(controller: BetController) extends SimpleRouter {
  val prefix = "/v1/bets"

  def link(id: Int): String = {
    import io.lemonlabs.uri.dsl._
    val url = prefix / id.toString
    url.toString()
  }

  override def routes: Routes = {
    case GET(p"/") =>
      controller.index

    case POST(p"/") =>
      controller.processBet

    case GET(p"/$id") =>
      controller.show(id)

    case POST(p"/$id/call") =>
      controller.processCall(id)
  }

}
