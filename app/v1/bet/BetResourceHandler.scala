package v1.bet

import exception.{BetCalledAlready, BetNotFound}
import javax.inject.{Inject, Provider}
import play.api.MarkerContext
import v1.user.UserRepository

import scala.concurrent.{ExecutionContext, Future}


class BetResourceHandler @Inject()(routerProvider: Provider[BetRouter],
                                   betRepository: BetRepositoryImpl,
                                   userRepository: UserRepository)(implicit ec: ExecutionContext) {

  def create(betInput: BetFormInput)(
    implicit mc: MarkerContext): Future[Bet] = {
    for {
      bet <- betRepository.create(betInput.firstUserId, betInput.firstPrediction, betInput.betMoney, betInput.stock,
        betInput.callExpirationDate, betInput.predictionExpirationDate)
      _ = userRepository.withdrawal(betInput.firstUserId, betInput.betMoney)
    } yield bet
  }


  def find(id: String)(implicit mc: MarkerContext): Future[Option[Bet]] = betRepository.find(id.toInt)

  def list(implicit mc: MarkerContext): Future[Iterable[Bet]] = {
    betRepository.list()
  }

  def call(id: String, callInput: BetCallFormInput)(
    implicit mc: MarkerContext): Future[Unit] = {
    for {
      bet <- betRepository.find(id.toInt).flatMap {
        case Some(bet) => {
          if (bet.secondUserId.isEmpty) {
            userRepository.withdrawal(callInput.secondUserId, bet.betMoney)
            betRepository.call(bet.id, callInput.secondUserId, callInput.secondUserPrediction)
          }
          else Future.failed(new BetCalledAlready(id.toInt))
        }
        case None => Future.failed(new BetNotFound(id.toInt))
      }
    } yield ()
  }
}
