package v1.bet

import java.time.LocalDateTime

import javax.inject.Inject
import play.api.Logger
import play.api.data.Form
import play.api.libs.json.Json
import play.api.mvc._

import scala.concurrent.{ExecutionContext, Future}

case class BetFormInput(firstUserId: Int, firstPrediction: BigDecimal, betMoney: Int, stock: String,
                        callExpirationDate: LocalDateTime, predictionExpirationDate: LocalDateTime)

case class BetCallFormInput(secondUserId: Int, secondUserPrediction: BigDecimal)

class BetController @Inject()(cc: BetControllerComponents)(
    implicit ec: ExecutionContext)
    extends BetBaseController(cc) {

  private val logger = Logger(getClass)

  private val betForm: Form[BetFormInput] = {
    import play.api.data.Forms._

    Form(
      mapping(
        "firstUserId" -> number,
        "firstPrediction" -> bigDecimal,
        "betMoney" -> number,
        "stock" -> nonEmptyText,
        "callExpirationDate" -> localDateTime,
        "predictionExpirationDate" -> localDateTime
      )(BetFormInput.apply)(BetFormInput.unapply)
    )
  }

  private val betCallForm: Form[BetCallFormInput] = {
    import play.api.data.Forms._

    Form(
      mapping(
        "secondUserId" -> number,
        "secondPrediction" -> bigDecimal,
      )(BetCallFormInput.apply)(BetCallFormInput.unapply)
    )
  }

  def index: Action[AnyContent] = BetAction.async { implicit request =>
    logger.trace("index: ")
    betResourceHandler.list.map { bets =>
      Ok(Json.toJson(bets))
    }
  }

  def processBet: Action[AnyContent] = BetAction.async { implicit request =>
    logger.trace("process bet: ")
    processJsonBetPost()
  }

  def show(id: String): Action[AnyContent] = BetAction.async { implicit  request =>
    logger.trace("show bet: ")
    betResourceHandler.find(id).map { bet =>
      Ok(Json.toJson(bet))
    }
  }

  def processCall(id: String): Action[AnyContent] = BetAction.async { implicit request =>
    logger.trace("process call: ")
    processJsonBetCallPost(id)
  }

  private def processJsonBetPost[A]()(
    implicit request: BetRequest[A]): Future[Result] = {
    def failure(badForm: Form[BetFormInput]) = {
      Future.successful(BadRequest(badForm.errorsAsJson))
    }

    def success(input: BetFormInput) = {
      betResourceHandler.create(input).map { bet =>
        Created(Json.toJson(bet)).withHeaders(LOCATION -> bet.id.toString)
      }
    }
    betForm.bindFromRequest().fold(failure, success)
  }

  private def processJsonBetCallPost[A](betId: String)(
    implicit request: BetRequest[A]): Future[Result] = {
    def failure(badForm: Form[BetCallFormInput]) = {
      Future.successful(BadRequest(badForm.errorsAsJson))
    }

    def success(input: BetCallFormInput) = {
      for {
        bet <- betResourceHandler.call(betId, input)
      } yield Accepted
    }
    betCallForm.bindFromRequest().fold(failure, success)
  }
}
