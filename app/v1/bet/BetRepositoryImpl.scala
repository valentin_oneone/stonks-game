package v1.bet

import java.time.LocalDateTime

import akka.actor.ActorSystem
import javax.inject.{Inject, Singleton}
import play.api.MarkerContext
import play.api.db.slick.DatabaseConfigProvider
import play.api.libs.concurrent.CustomExecutionContext
import play.api.libs.json.{Json, OFormat}
import slick.jdbc.JdbcProfile

import scala.concurrent.{ExecutionContext, Future}
import scala.math.abs



case class Bet(id: Int,
               firstUserId: Int,
               firstPrediction: BigDecimal,
               secondUserId: Option[Int],
               secondPrediction: Option[BigDecimal],
               betMoney: Int,
               stock: String,
               callExpirationDate: LocalDateTime,
               predictionExpirationDate: LocalDateTime,
               betStatus: String,
               winnerId: Option[Int]) {

  def getWinnerId(realPrice: BigDecimal): Int = {
    Seq(firstUserId -> (firstPrediction - realPrice).abs,
      secondUserId.get -> (secondPrediction.get - realPrice).abs).minBy(_._2)._1
  }
}

object Bet {
  implicit val betFormat: OFormat[Bet] = Json.format[Bet]
}

class BetExecutionContext @Inject()(actorSystem: ActorSystem)
  extends CustomExecutionContext(actorSystem, "repository.dispatcher")

trait BetRepository {
  def create(firstUserId: Int,
             firstUserPrediction: BigDecimal,
             betMoney: Int,
             stock: String,
             callExpirationDate: LocalDateTime,
             predictionExpirationDate: LocalDateTime)(implicit mc: MarkerContext): Future[Bet]

  def list()(implicit mc: MarkerContext): Future[Seq[Bet]]

  def find(id: Int)(implicit mc: MarkerContext): Future[Option[Bet]]

  def call(id: Int, secondUserId: Int, secondUserPrediction: BigDecimal)(implicit mc: MarkerContext): Future[Int]

  def close(id: Int)(implicit mc: MarkerContext): Future[Int]

  def fail(id: Int)(implicit mc: MarkerContext): Future[Int]

  def setWinner(betId: Int, winnerId: Int)(implicit mc: MarkerContext): Future[Int]

  def listCalledByDate(date: LocalDateTime)(implicit mc: MarkerContext): Future[Seq[Bet]]

  def listNotCalledByDate(date: LocalDateTime)(implicit mc: MarkerContext): Future[Seq[Bet]]
}

@Singleton
class BetRepositoryImpl @Inject()(dbConfigProvider: DatabaseConfigProvider)(implicit ec: ExecutionContext) extends BetRepository {
  private val dbConfig = dbConfigProvider.get[JdbcProfile]

  import dbConfig._
  import profile.api._

  private class BetTable(tag: Tag) extends Table[Bet](tag, "bets") {
    def id: Rep[Int] = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def firstUserId: Rep[Int] = column[Int]("first_user_id")
    def firstPrediction: Rep[BigDecimal] = column[BigDecimal]("first_prediction")
    def secondUserId: Rep[Option[Int]] = column[Option[Int]]("second_user_id")
    def secondPrediction: Rep[Option[BigDecimal]] = column[Option[BigDecimal]]("second_prediction")
    def betMoney: Rep[Int] = column[Int]("bet_money")
    def stock: Rep[String] = column[String]("stock")
    def callExpirationDate: Rep[LocalDateTime] = column[LocalDateTime]("call_expiration_date")
    def predictionExpirationDate: Rep[LocalDateTime] = column[LocalDateTime]("pred_expiration_date")
    def betStatus: Rep[String] = column[String]("bet_status")
    def winnerId: Rep[Option[Int]] = column[Option[Int]]("winner_id")

    override def * = (id, firstUserId, firstPrediction, secondUserId,
      secondPrediction, betMoney, stock, callExpirationDate, predictionExpirationDate, betStatus, winnerId) <>
      ((Bet.apply _).tupled, Bet.unapply)
  }

  private val bets = TableQuery[BetTable]

  def create(firstUserId: Int, firstPrediction: BigDecimal, betMoney: Int, stock: String,
                      callExpirationDate: LocalDateTime, predictionExpirationDate: LocalDateTime)
                     (implicit mc: MarkerContext): Future[Bet] = db.run {
    (bets returning bets.map(_.id)
      into((bet, id) => bet.copy(id = id))
    ) += Bet(id = 0, firstUserId,firstPrediction, Option.empty, Option.empty, betMoney, stock,
      callExpirationDate, predictionExpirationDate, "Open", Option.empty)
  }

  def list()(implicit mc: MarkerContext): Future[Seq[Bet]] = db.run {
    bets.result
  }

  def find(id: Int)(implicit mc: MarkerContext): Future[Option[Bet]] = db.run {
    bets.filter(_.id === id).result.headOption
  }

  def call(id: Int, secondUserId: Int, secondUserPrediction: BigDecimal)(implicit mc: MarkerContext): Future[Int] = db.run {
    bets.filter(_.id === id)
      .map(bet => (bet.secondUserId, bet.secondPrediction, bet.betStatus))
      .update((Some(secondUserId), Some(secondUserPrediction), "Called"))
  }

  def close(id: Int)(implicit mc: MarkerContext): Future[Int] = db.run {
    bets.filter(_.id === id)
      .map(bet => bet.betStatus)
      .update("Closed")
  }

  def fail(id: Int)(implicit mc: MarkerContext): Future[Int] = db.run {
    bets.filter(_.id === id)
      .map(bet => bet.betStatus)
      .update("Failed")
  }

  def listCalledByDate(date: LocalDateTime)(implicit mc: MarkerContext): Future[Seq[Bet]] = db.run {
    bets
      .filter(bet => bet.betStatus === "Called")
      .filter(bet => bet.predictionExpirationDate <= date)
      .result
  }

  def listNotCalledByDate(date: LocalDateTime)(implicit mc: MarkerContext): Future[Seq[Bet]] = db.run {
    bets
      .filter(bet => bet.callExpirationDate <= date)
      .filter(bet => bet.betStatus === "Open")
      .result
  }

  def setWinner(betId: Int, winnerId: Int)(implicit mc: MarkerContext): Future[Int] = db.run {
    bets
      .filter(_.id === betId)
      .map(bet => (bet.betStatus, bet.winnerId))
      .update(("Closed", Some(winnerId)))
  }
}

