package actor

import java.time.LocalDateTime

import akka.actor.{Actor, ActorSystem, Props}
import com.google.inject.{Inject, Singleton}
import play.api.Logger
import v1.bet.BetRepository
import v1.user.UserRepository

object BetCloseNotCalledActor {
  def props = Props[BetWinnerCheckerActor]

  case class CloseNotCalledBets()
}

@Singleton
class BetCloseNotCalledActor @Inject()(betRepo: BetRepository, userRepo: UserRepository) extends Actor {
  import BetCloseNotCalledActor._

  private val logger = Logger(getClass)
  implicit val system: ActorSystem = context.system
  import context.dispatcher

  override def receive: Receive = {
    case CloseNotCalledBets() => {
      logger.trace("start close not called bets: ")
      val time = LocalDateTime.now()
      val bets = betRepo.listNotCalledByDate(time)
      bets.foreach(bet => logger.trace(s"closing bets: $bet"))
      val betsToClose = bets.map(bets => bets.map(bet => {
        betRepo.close(bet.id)
        userRepo.refill(bet.firstUserId, bet.betMoney)
      }))
    }
  }
}
