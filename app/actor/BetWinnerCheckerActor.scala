package actor

import java.time.{LocalDateTime, ZoneId, ZonedDateTime}
import java.time.format.DateTimeFormatter

import akka.actor.{Actor, ActorSystem, Props}
import com.google.inject.{Inject, Singleton}
import exception.{CandleNotFound, FigiNotFound}
import play.api.libs.ws.WSClient
import play.api.{Configuration, Logger}
import v1.bet.{Bet, BetRepository}
import v1.stock.{CandlesResponse, StockRepository}
import utils.OptionExtension._
import v1.user.UserRepository

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.{Failure, Success}

object BetWinnerCheckerActor {
  def props = Props[BetWinnerCheckerActor]

  case class CheckBetsStatus()

}

@Singleton
class BetWinnerCheckerActor @Inject()(config: Configuration, ws: WSClient, userRepo: UserRepository,
                                      betRepo: BetRepository, stockRepo: StockRepository) extends Actor {

  import BetWinnerCheckerActor._

  private val logger = Logger(getClass)
  implicit val system: ActorSystem = context.system

  override def receive: Receive = {
    case CheckBetsStatus() =>
      logger.trace("start check winner in bets: ")
      val time = LocalDateTime.now()
      val token: String = config.get[String]("openAPIConfig.OpenAPIToken")

      for {
        bets <- betRepo.listCalledByDate(time.minusMinutes(1))
        _ = logger.trace(s"check winner in bets $bets")
        _ <- Future.traverse(bets){ bet =>
          logger.trace(s"Начинаю обрабатывать ставку $bet")
          val finalCandleFuture = for {
            figiOpt <- stockRepo.getFigi(bet.stock)
            figi <- figiOpt.asFutureOrDie(new FigiNotFound(bet.stock, bet.id))
            candleResponse <- fetchCandleResponse(bet, figi, token)
            betFinalCandleOpt = candleResponse.payload.filterNot(_.time.isAfter(bet.predictionExpirationDate)).maxByOption(_.time)
            betFinalCandle <- betFinalCandleOpt.asFutureOrDie(new CandleNotFound(bet.id))
            _ = logger.trace(s"Найдена подходящая свеча $betFinalCandle")
          } yield betFinalCandle
          finalCandleFuture.transformWith{
            case Success(value) =>
              val winnerId = bet.getWinnerId(value.c.toInt)
              logger.trace(s"Для ставки ${bet.id} определён победитель $winnerId. ")
              betRepo.setWinner(bet.id, winnerId)
              userRepo.refill(winnerId, bet.betMoney * 2)
            case Failure(exception) =>
              logger.error(s"Ошибка при определении победителя ставки ${bet.id}", exception)
              betRepo.fail(bet.id)
          }
        }
      } yield ()
  }

  private def fetchCandleResponse(bet: Bet, figi: String, token: String): Future[CandlesResponse] = {
    ws.url(createUrl())
      .withQueryStringParameters(createQueryParams(figi, bet.predictionExpirationDate.minusMinutes(60), bet.predictionExpirationDate, "1min") :_*)
      .withHttpHeaders(("Authorization", s"Bearer $token")).get().map{ response =>
      logger.trace(response.json.toString)
      response.json.as[CandlesResponse]
    }
  }

  private def createUrl(): String = {
    s"https://api-invest.tinkoff.ru/openapi/market/candles"
  }

  private def createQueryParams(figi: String, from: LocalDateTime, to: LocalDateTime, interval: String): Seq[(String, String)] = {
    val formatter: DateTimeFormatter = DateTimeFormatter.ISO_OFFSET_DATE_TIME
    logger.trace(ZonedDateTime.of(from, ZoneId.of("Europe/Moscow")).format(formatter))
    Seq("figi" -> figi, "from" -> ZonedDateTime.of(from, ZoneId.of("Europe/Moscow")).format(formatter),
      "to" -> ZonedDateTime.of(to, ZoneId.of("Europe/Moscow")).format(formatter), "interval" -> interval)
  }
}
