package exception

import play.api.UsefulException
import play.api.libs.json.{Json, OWrites}

case class StonksException(errorCode: String, errorMessage: String) extends UsefulException(errorMessage)

class BadRequestException(errorCode: String, errorMessage: String) extends StonksException(errorCode, errorMessage)
class InternalServerException(errorCode: String, errorMessage: String) extends StonksException(errorCode, errorMessage)

class BetNotFound(betId: Int) extends BadRequestException("BET_NOT_FOUND", s"Ставка $betId не найдена")
class BetCalledAlready(betId: Int) extends BadRequestException("BET_CALLED_ALREADY", s"Ставку $betId поддержал кто-то другой")
class FigiNotFound(ticket: String, betId: Int) extends InternalServerException("FIGI_NOT_FOUND", s"Figi не найден для $ticket в ставке $betId")
class CandleNotFound(betId: Int) extends InternalServerException("CANDLE_NOT_FOUND", s"Не найдено свеч для ставки $betId")

class NotEnoughMoney(userId: Int) extends BadRequestException("NOT_ENOUGH_MONEY", s"У пользователя $userId недостаточно средств ")
class UserNotFound(userId: Int) extends BadRequestException("USER_NOT_FOUND", s"Юзер $userId не найден")

class UnknownException() extends InternalServerException("UNKNOWN_EXCEPTION", "Неизвестная ошибка")

object StonksException {
  implicit val writes: OWrites[StonksException] = Json.writes[StonksException]
}
