package utils

import scala.concurrent.Future

object OptionExtension {
  implicit class OptionToFuture[A](val option: Option[A]) extends AnyVal {
    def asFutureOrDie(ifEmpty: => Throwable): Future[A] = {
      option.fold[Future[A]](Future.failed(ifEmpty))(Future.successful)
    }
  }
}
