import com.google.inject.AbstractModule
import javax.inject._
import net.codingwell.scalaguice.ScalaModule
import play.api.{Configuration, Environment}
import v1.bet.{BetRepository, BetRepositoryImpl}
import v1.stock.{StockRepository, InMemoryStockRepositoryImpl}
import v1.user.{UserRepository, UserRepositoryImpl}

class Module(environment: Environment, configuration: Configuration)
  extends AbstractModule
    with ScalaModule {

  override def configure() = {
    bind[UserRepository].to[UserRepositoryImpl].in[Singleton]
    bind[BetRepository].to[BetRepositoryImpl].in[Singleton]
    bind[StockRepository].to[InMemoryStockRepositoryImpl].in[Singleton]
    bind[ApplicationStart].asEagerSingleton()
  }
}
