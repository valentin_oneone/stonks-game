import actor.{BetWinnerCheckerActor, BetCloseNotCalledActor, GuiceActorProducer}
import akka.actor.{ActorRef, ActorSystem, Props}
import com.google.inject.Inject
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension
import play.api.inject.ApplicationLifecycle
import play.inject.Injector

class ApplicationStart @Inject()(
                                  lifecycle: ApplicationLifecycle,
                                  system: ActorSystem,
                                  injector: Injector
                                ) {

  // Start scheduling
  val scheduler: QuartzSchedulerExtension = QuartzSchedulerExtension(system)
  val betWinnerReceiver: ActorRef = system.actorOf(Props.create(classOf[GuiceActorProducer], injector, classOf[BetWinnerCheckerActor]))
  val betNotCalledReceiver: ActorRef = system.actorOf(Props.create(classOf[GuiceActorProducer], injector, classOf[BetCloseNotCalledActor]))
  scheduler.schedule("betWinnerChecker", betWinnerReceiver, BetWinnerCheckerActor.CheckBetsStatus(), None)
  scheduler.schedule("betNotCalledChecker", betNotCalledReceiver, BetCloseNotCalledActor.CloseNotCalledBets(), None)
}